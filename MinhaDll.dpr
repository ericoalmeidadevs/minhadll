library MinhaDll;

uses
  System.SysUtils,
  System.Classes;

{$R *.res}

function Somar(AValueOne: currency; AValueTwo: currency): currency; stdcall;
begin
  Result := AValueOne + AValueTwo;
end;

function Subtrair(AValueOne: currency; AValueTwo: currency): currency; stdcall;
begin
  Result := AValueOne - AValueTwo;
end;

function Multiplicar(AValueOne: currency; AValueTwo: currency): currency; stdcall;
begin
  Result := AValueOne * AValueTwo;
end;

function Dividir(AValueOne: currency; AValueTwo: currency): currency; stdcall;
begin
  Result := AValueOne / AValueTwo;
end;

function Desconto(AValue: currency; ADesconto: currency): currency; stdcall;
begin
  Result := ((AValue * ADesconto)/100);
end;

function DescontoConcedido(AValue: currency; ADesconto: currency): currency; stdcall;
begin
  Result := ((ADesconto/AValue)*100);
end;


exports
  Somar,
  Subtrair,
  Multiplicar,
  Dividir,
  Desconto,
  DescontoConcedido;

begin

end.
